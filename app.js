import webpack from 'webpack';
import path from 'path';
import config from './webpack.config.dev';
var cluster = require('cluster');
var memored = require('memored');
var sticky = require('sticky-session');
var express = require('express');
var app = express();
const compiler = webpack(config);
const r = require('rethinkdb')




app.get('/', function (req, res) {
  console.log('worker: ' + cluster.worker.id);
  res.sendFile(path.join(__dirname, './src/index.html'));
});

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

var server = require('http').createServer(app);


if (!sticky.listen(server, 3001)) {
  // Master code
  if (cluster.isMaster) {


    cluster.on('fork', function (worker) {
      console.log('worker %s created', worker.id);
    });
    cluster.on('online', function (worker) {
      console.log('worker %s online', worker.id);
    });
    cluster.on('listening', function (worker, addr) {
      console.log('worker %s listen on %s:%d', worker.id, addr.address, addr.port);
    });
    cluster.on('disconnect', function (worker) {
      console.log('worker %s disconnected', worker.id);
    });
    cluster.on('exit', function (worker, code, signal) {
      console.log('worker %s killed (%s)', worker.id, signal || code);
      console.log('new worker %s created', worker.id);
      cluster.fork();
    });
  }

  server.once('listening', function () {
    console.log('server started on 3001 port');
  });
} else {
  // Worker code
  var io = require('socket.io').listen(server)
  const redis = require('socket.io-redis');

  io.adapter(redis({
    host: 'localhost',
    port: 6379
  }));

  function intersects(rectA, rectB) {
    return !(rectA.x + rectA.width < rectB.x ||
      rectB.x + rectB.width < rectA.x ||
      rectA.y + rectA.height < rectB.y ||
      rectB.y + rectB.height < rectA.y)
  }

  function resetBall({
    socket,
    game
  }) {
    game.speed = 0.01;
    game.ball = {
      x: 0.5,
      y: 0.5
    };
    game.directionball.x = 0;
    game.directionball.y = 0;
    while (game.directionball.x == 0 || game.directionball.y == 0) {
      game.directionball.x = Math.floor((Math.random() * 3) - 1);
      game.directionball.y = Math.floor((Math.random() * 3) - 1);
    }
    return game;
  }


  function updateGame(socket) {
    console.log('update game')
    r.table('room').filter(r.row('roomId').eq(socket.room)).
    run(connection, function (err, cursor) {
      if (err) throw err
      cursor.toArray(function (err, result) {
        if (err) throw err;
        let game = result[0].game
        // console.log("GAME")
        // console.log(game)
        // timegame[socket.room] += 1;
        // if (timegame[socket.room] < 100) {
        //   var getready = (0 + timegame[socket.room]) + "%";
        //   socket.emit('getReady', {
        //     getready
        //   });
        //   socket.broadcast.emit('getReady', {
        //     getready
        //   });
        //   return;
        // } else
        if (game.scores.p2 < 10 && game.scores.p1 < 10) {
          if (!game.ballReseted || game.ballReseted == false) {
            game = resetBall({
              socket,
              game
            });
            game.ballReseted = true;
          }
          game.ball.x += game.directionball.x * game.speed;
          game.ball.y += game.directionball.y * game.speed;
          if (game.ball.x <= 0 || game.ball.x >= 1) {
            if (game.ball.x <= 0) {
              game.scores.p2 += 1;
            } else {
              game.scores.p1 += 1;
            }
            game = resetBall({
              socket,
              game
            })
          }
          if (game.ball.y <= 0 || game.ball.y >= 1) {
            game.directionball.y = -game.directionball.y
          }
          if (
            intersects({
              x: 0.1,
              y: game.player1,
              width: 0.025,
              height: 0.2
            }, {
              x: game.ball.x,
              y: game.ball.y,
              width: 0.01,
              height: 0.01
            }) ||
            intersects({
              x: 0.87,
              y: game.player2,
              width: 0.025,
              height: 0.2
            }, {
              x: game.ball.x,
              y: game.ball.y,
              width: 0.01,
              height: 0.01
            })
          ) {
            game.speed += 0.001
            game.directionball.x = -game.directionball.x
          }

          r.table('room').filter(r.row('roomId').eq(socket.room))
            .update({
              'game': game
            })
            .run(connection, function (err, result) {
              if (err) throw err;
              // console.log('UPDATe')
              // console.log(game)
              // console.log(JSON.stringify(result, null, 2));
            })


          //   r.table('room').changes().run(connection, function(err, cursor) {
          //     if (err) throw err;
          //     cursor.each(function(err, row) {
          //         if (err) throw err;
          //         io.sockets.in(socket.room).emit('updateGame', {game: row.new_val});
          //         console.log(JSON.stringify(row, null, 2));
          //     });
          // });

          // console.log(game)
        } else {

          ballReseted[socket.room] = false;
          game.status = 0;
          timegame[socket.room] = 0
          game = {
            directionball: {
              x: 1,
              y: 1
            },
            ball: {
              x: 0.5,
              y: 0.5
            },
            player1: 0.5,
            player2: 0.5,
            scores: {
              p1: 0,
              p2: 0
            },
            speed: 0.01
          }

          if (game.scores.p1 == 10)
            game.winner = game.players[0]
          else if (game.scores.p2 == 10)
            game.winner = game.players[1]

          r.table('room').filter(r.row('roomId').eq(socket.room))
            .update({
              'game': game
            })
            .run(connection, function (err, result) {
              if (err) throw err;
              // console.log('UPDATe')
              // console.log(game)
              // io.sockets.in(socket.room).emit('updateGame', {game: row.new_val});
              // console.log(JSON.stringify(result, null, 2));
            })



          //   r.table('room').changes().run(connection, function(err, cursor) {
          //     if (err) throw err;
          //     cursor.each(function(err, row) {
          //         if (err) throw err;
          //         console.log(row.new_val)
          //         io.sockets.in(socket.room).emit('updateGame', {game: row.new_val});
          //         console.log(JSON.stringify(row, null, 2));
          //     });
          // });

          // clearInterval(gameInterval[socket.room]);
        }

      })

    })
  }

  let connection = null
  r.connect({
    host: 'localhost',
    port: 28015,
    db: 'pingpong'
  }, function (err, conn) {
    connection = conn
    r.db('pingpong').table('room').delete().run(connection, function (err, result) {
      if (err) throw err;
    })
    if (err) throw err;

    io.on('connection', function (socket) {
      console.log('a user connected / Num :' + socket.id + ' on worker ' + cluster.worker.id);


      socket.on('update', (data) => {
        if (socket.room == data.room) {
          games[socket.room] = data.game
          console.log('update ' + socket.username)
        }
      })

      socket.on('verify', (username, fn) => {
        console.log("Verifiying Name")
        let exist = false
        r.table('room').run(conn).then(function (cursor) {
          return cursor.toArray()
        }).then(function (results) {
          console.log(results)
          if (results.length > 0) {
            results.map(room => {
              if (room.players.includes(username))
                exist = true
            })
          }
        }).then(function () {
          console.log("Verifiying FIn")
          fn(exist)
        })
        .catch(function (err) {
        console.log(err)
      })

      // let rooms = io.sockets.adapter.rooms;
      // io.of('/').adapter.allRooms((err, rooms) => {
      //   for (let room in rooms) {
      //     for (let client in playerNames[room]) {
      //       if (playerNames[room][client] == username)
      //         exist = true
      //     }
      //   }

      // })
    })

    socket.on('addUser', function (username) {
      socket.username = username;
      r.table('room').insert({
        roomId: socket.id,
        username,
        players: [username]
      }).run(conn, function (err, result) {
        if (err) throw err;
        console.log(JSON.stringify(result, null, 2));
      })
      // playerNames[socket.id] = [socket.username]
      // socket.playerNames = playerNames
      // memored.store('name' + socket.id, {
      //   name: socket.username
      // }, function () {});

      // console.log('NAAAAAAAAAAAAAAAAAAAAAAME ' + socket.username)
    });

    socket.on('playerUp', (data) => {
      // if(socket.room == data.room) {
      console.log("UP")
      console.log(data)
      console.log(socket.room)
      r.table('room').filter(r.row('roomId').eq(data.room)).
      run(connection, function (err, cursor) {
        if (err) throw err
        cursor.toArray(function (err, result) {
          if (err) throw err;
          console.log(result)
          let room = result[0]
          let game = result[0].game
          if (data.username == room.players[0]) {
            if (game.player1 <= 0) return;
            game.player1 -= 0.02
            console.log("Up " + data.username + ' ' + game.player1)
          } else if (data.username == room.players[1]) {
            if (game.player2 <= 0) return;
            game.player2 -= 0.02
            console.log("Up " + data.username + ' ' + game.player2)
          }
          r.table('room').filter(r.row('roomId').eq(data.room))
            .update({
              'game': game
            })
            .run(connection, function (err, result) {
              if (err) throw err;
              // io.sockets.in(socket.room).emit('updateGame', {game: row.new_val});
              // console.log(JSON.stringify(result, null, 2));
            })
        })
      })
      // }
    });

    socket.on('playerDown', (data) => {
      // if(socket.room == data.room) {
      r.table('room').filter(r.row('roomId').eq(data.room)).
      run(connection, function (err, cursor) {
        if (err) throw err
        cursor.toArray(function (err, result) {
          if (err) throw err;
          let room = result[0]
          let game = result[0].game
          if (data.username == room.players[0]) {
            if (game.player1 >= 0.8) return;
            game.player1 += 0.02
          } else if (data.username == room.players[1]) {
            if (game.player2 >= 0.8) return;
            game.player2 += 0.02
          }
          r.table('room').filter(r.row('roomId').eq(data.room))
            .update({
              'game': game
            })
            .run(connection, function (err, result) {
              if (err) throw err;
              // io.sockets.in(socket.room).emit('updateGame', {game: row.new_val});
              console.log(JSON.stringify(result, null, 2));
            })
        })
      })
      // }
    });

    socket.on('leave', () => {
      // playerNames[socket.room] = playerNames[socket.room].filter(player => player !== socket.username)
      socket.leave(socket.room)
    })

    socket.on('replay', (username, fn) => {
      console.log(username)
      if (replayPlayers[socket.room])
        replayPlayers[socket.room].push(username)
      else replayPlayers[socket.room] = [username]
      console.log(replayPlayers[socket.room])
      if (replayPlayers[socket.room].length == 2) {
        gameInterval[socket.room] = setInterval(() => {
          updateGame(socket);
        }, 50);
        emitingGame[socket.room] = true;
        fn(2)
      }
      if (playerNames[socket.room].length == 1)
        fn(1)
    })


    socket.on('sendGame', (data) => {
      if (socket.room == data.room) {
        console.log("Connected" + socket.username + " To " + data.room)
        socket.room = data.room
        // games[data.room] = data.game
        // playerNames[data.room] = data.playerNames
      }
    })

    socket.on('join', () => {

      socket.room = socket.id
      let available = false

      r.table('room').filter(r.row('roomId').ne(socket.id)).
      run(connection, function (err, cursor) {
        if (err) throw err;
        cursor.toArray(function (err, result) {
          if (err) throw err;
          console.log('RESULT')
          console.log(result)
          result.forEach(function (room, index) {
            console.log("LENGTH")
            console.log(room.players.length)
            if (room.players.length == 1 && !available) {
              socket.join(room.roomId);
              socket.room = room.roomId
              available = true
              r.table('room').filter(r.row('roomId').eq(room.roomId))
                .update({
                  'players': r.row("players").append(socket.username),
                  'game': {
                    status: 0,
                    directionball: {
                      x: 1,
                      y: 1
                    },
                    ball: {
                      x: 0.5,
                      y: 0.5
                    },
                    player1: 0.5,
                    player2: 0.5,
                    scores: {
                      p1: 0,
                      p2: 0
                    },
                    speed: 0.01
                  }
                })
                .run(connection, function (err, result) {
                  if (err) throw err;
                  console.log(JSON.stringify(result, null, 2));
                  r.table('room').filter(r.row('roomId').eq(room.roomId))
                    .run(connection, function (err, cursor) {
                      cursor.toArray(function (err, result) {
                        let newRoom = result[0]
                        console.log("NEW ROOM")
                        console.log(newRoom)
                        r.table("room").filter({
                          "roomId": socket.id
                        }).delete().run(conn, function (err, result) {
                          if (err) throw err;
                          socket.leave(socket.id)
                          if (!newRoom.game.status || newRoom.game.status == 0) {
                            r.table('room').filter(r.row('roomId').eq(newRoom.roomId))
                              .update({
                                'players': r.row("players"),
                                'game': {
                                  status: 1,
                                  directionball: {
                                    x: 1,
                                    y: 1
                                  },
                                  ball: {
                                    x: 0.5,
                                    y: 0.5
                                  },
                                  player1: 0.5,
                                  player2: 0.5,
                                  scores: {
                                    p1: 0,
                                    p2: 0
                                  },
                                  speed: 0.01
                                }
                              })
                              .run(connection, function (err, result) {
                                if (err) throw err;
                                // gameInterval[room] = setInterval(() => {
                                // }, 50);

                                io.sockets.in(newRoom.roomId).emit('connectToRoom', {
                                  players: 2,
                                  names: newRoom.players,
                                  game: newRoom.game,
                                  room: newRoom.roomId
                                })

                                socket.interval = setInterval(() => {
                                  updateGame(socket);
                                }, 50);

                                r.table('room').get(newRoom.id).changes().run(connection, function (err, cursor) {
                                  if (err) throw err;
                                  cursor.each(function (err, row) {
                                    if (err) throw err;
                                    if (row.new_val.roomId == socket.room) {
                                      io.sockets.in(socket.room).emit('updateGame', {
                                        game: row.new_val.game
                                      });
                                    }
                                    // console.log(JSON.stringify(row, null, 2));
                                  });
                                });

                              })


                          }
                        })


                      })

                    })
                });
            }

          })
          // console.log(JSON.stringify(result, null, 2));
        });
      });

      // io.of('/').adapter.allRooms((err, rooms) => {
      //   // console.log(rooms);
      //   // console.log('///////')
      //   for (let r in rooms) {
      //     // console.log(r)
      //     // console.log(rooms[r])
      //     let room = rooms[r]

      //     io.of('/').in(room).clients((error, clients) => {

      //       console.log('clients');
      //       console.log(clients); // => [Anw2LatarvGVVXEIAAAD] 

      //       if (room == socket.id) {
      //         playerNames[room] = [socket.username]
      //         socket.playerNames = playerNames
      //         console.log("Player ! ")
      //         console.log(playerNames)
      //       }

      //       if (clients.length == 1 && !available && room !== socket.id) {
      //         available = true

      //         console.log("Join")
      //         socket.join(room);
      //         console.log(room)
      //         console.log("Player ! ")
      //         console.log(playerNames)
      //         console.log("Join/>")

      //         socket.room = room

      //         memored.read('name' + room, function (err, value) {
      //           console.log('Read value:', value);
      //           playerNames[room] = [value.name]
      //           playerNames[room].push(socket.username)
      //           socket.playerNames = playerNames
      //           emitingGame[room] = false
      //           ballReseted[room] = false
      //           timegame[room] = 0
      //           console.log("Room PlayerNames :")
      //           console.log(playerNames[room])
      //           memored.store('name' + room, {
      //             name: playerNames[room]
      //           }, function (err, value) {})
      //         });

      //         // if (playerNames[room]) {
      //         // playerNames[room].push(socket.username)
      //         // emitingGame[room] = false
      //         // ballReseted[room] = false
      //         // timegame[room] = 0
      //         // console.log("Room PlayerNames :")
      //         // console.log(playerNames[room])
      //         // }

      //         // else playerNames[room] = [socket.username]

      //         io.of('/').in(room).clients((error, clients2) => {
      //           console.log("Clientss2")
      //           console.log(clients2)
      //           console.log('Length : ' + clients2.length)
      //           if (clients2.length == 2) {
      //             games[room] = {
      //               directionball: {
      //                 x: 1,
      //                 y: 1
      //               },
      //               ball: {
      //                 x: 0.5,
      //                 y: 0.5
      //               },
      //               player1: 0.5,
      //               player2: 0.5,
      //               scores: {
      //                 p1: 0,
      //                 p2: 0
      //               },
      //               speed: 0.01
      //             }
      //             io.sockets.in(room).emit('connectToRoom', {
      //               players: 2,
      //               names: playerNames[room],
      //               game: games[room],
      //               room
      //             })
      //             if (emitingGame[room] == false || !emitingGame[room]) {
      //               gameInterval[room] = setInterval(() => {
      //                 updateGame(socket);
      //               }, 50);
      //               emitingGame[room] = true;
      //             }
      //           }
      //         })
      //         // socket.join(room);

      //         if (room !== socket.id) {
      //           socket.leave(socket.id);
      //         }

      //       }
      //     });


      //   }

      //   // console.log(rooms)
      // });



      ///////////// Without Cluster /////////////////


      //   let available = false
      //   console.log(availableRooms)
      //   console.log('///////')

      //   socket.room = socket.id

      //   availableRooms[socket.room] = socket.room


      //   // if any of the current rooms have only one
      //   // player, join that room.
      //   for (let room in availableRooms) {
      //     //  console.log(availableRooms)
      //       if ( (availableRooms[room].length == 1 && !available)) {

      //         available = true
      //         socket.join(room);
      //         socket.room = room

      //         if (playerNames[room]) {
      //           playerNames[room].push(socket.username)
      //           emitingGame[room] = false
      //           ballReseted[room] = false
      //           timegame[room] = 0
      //           console.log(playerNames[room])
      //         }

      //         else playerNames[room] = [socket.username]

      //         if (availableRooms[room].length == 2 ) {
      //           games[room] = { 
      //               directionball:{x:1,y:1},
      //               ball:{x:0.5,y:0.5},
      //               player1:0.5,
      //               player2:0.5,
      //               scores:{p1:0,p2:0},
      //               speed: 0.01
      //             }
      //           io.sockets.in(room).emit('connectToRoom', {players:2, names:playerNames[room]})
      //           if(emitingGame[room]==false){
      //             gameInterval[room] = setInterval(() => {
      //               updateGame(socket);
      //             }, 50);
      //             emitingGame[room] = true;
      //           }
      //         }

      //         if (room!==socket.id) {
      //           socket.leave(socket.id);
      //         }
      //   }
      // } 

      // availableRooms[socket.room] = socket.room

      //   console.log(availableRooms)

    })

    socket.on('joinOther', () => {
      playerNames[socket.room] = playerNames[socket.room].filter(player => player !== socket.username)
      socket.leave(socket.room)
      let Oldromm = socket.room
      let rooms = io.sockets.adapter.rooms;
      let available = false
      console.log(rooms)
      console.log('///////')
      socket.room = socket.id

      // if any of the current rooms have only one
      // player, join that room.
      for (let room in rooms) {
        if (io.of('/').adapter.rooms[room].length == 1 && !available && room !== Oldromm) {
          available = true
          socket.join(room);
          socket.room = room

          if (playerNames[room]) {
            playerNames[room].push(socket.username)
            emitingGame[room] = false
            ballReseted[room] = false
            timegame[room] = 0
            console.log(playerNames[room])
          } else playerNames[room] = [socket.username]

          if (io.of('/').adapter.rooms[room].length == 2) {
            games[room] = {
              directionball: {
                x: 1,
                y: 1
              },
              ball: {
                x: 0.5,
                y: 0.5
              },
              player1: 0.5,
              player2: 0.5,
              scores: {
                p1: 0,
                p2: 0
              },
              speed: 0.01
            }
            io.sockets.in(room).emit('connectToRoom', {
              players: 2,
              names: playerNames[room]
            })
            if (emitingGame[room] == false) {
              gameInterval[room] = setInterval(() => {
                updateGame(socket);
              }, 50);
              emitingGame[room] = true;
            }
          }

          if (room !== socket.id) {
            socket.leave(socket.id);
          }

        }
      }

      console.log(rooms)
    })

    socket.on('disconnect', function () {
      console.log('user disconnected');

      // r.table('room').filter(
      //   (function (room) {return (room('players').contains(socket.username) && room('players').count().lt(2)) })
      // )
      // .delete().run(connection, function (err, result) {
      //   if (err) throw err;
      //   console.log(JSON.stringify(result, null, 2));
      //   console.log("CHANGED LT 1")
      // });

      r.table('room').filter(
          (function (room) {
            return (room('players').contains(socket.username) && room('players').count().gt(1))
          }))
        .update(function (row) {
          return {
            'players': row('players')
              .filter(function (item) {
                return item.ne(socket.username)
              }),
            'game': {
              status: 0,
              directionball: {
                x: 1,
                y: 1
              },
              ball: {
                x: 0.5,
                y: 0.5
              },
              player1: 0.5,
              player2: 0.5,
              scores: {
                p1: 0,
                p2: 0
              },
              speed: 0.01
            }
          }
        })
        .run(connection, function (err, result) {
          if (err) throw err;
          console.log(JSON.stringify(result, null, 2));
          console.log('CHANGED')
        });



      // playerNames[socket.room] = playerNames[socket.room].filter(player => player !== socket.username)
      io.sockets.in(socket.room).emit('playerLeft', {
        Left: socket.username
      })

      clearInterval(socket.interval);
    });
  });

});


}
