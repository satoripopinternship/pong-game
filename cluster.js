import express from 'express';
import webpack from 'webpack';
import path from 'path';
import config from './webpack.config.dev';
import open from 'open';
const puppeteer = require('puppeteer');
require('dotenv').config();
const NODE_ENV = process.env.NODE_ENV || 'development'
const port =  process.env.PORT || 1337 
const HOST = process.env.BASE_URL || 'localhost';
const baseUrl = `http://${HOST}:${port}`;

/* eslint-disable no-console */

const app = express();
const compiler = webpack(config);
let thisBrowser = null
let thisPage = null
let number =0


app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('*', function(req, res) {
  res.sendFile(path.join( __dirname, './src/index.html'));
});


  var server = app.listen(port , function () {
      console.log(`Express server listening on ${port}`)
      console.log(`${process.env.PORT}`);
      console.log(`${process.env.NODE_ENV}`);
  });

var io = require('socket.io').listen(server)

let games = {}
let playerNames = {}
let ballReseted = {}
let emitingGame = {}
let gameInterval = {}
let timegame = {}
let replayPlayers = {}

function intersects(rectA, rectB) {
  return !(rectA.x + rectA.width < rectB.x ||
           rectB.x + rectB.width < rectA.x ||
           rectA.y + rectA.height < rectB.y ||
           rectB.y + rectB.height < rectA.y)
}

function resetBall(socket) {
  let game = games[socket.room]
  game.speed = 0.01;
  game.ball={x:0.5,y:0.5};
  game.directionball.x = 0;
  game.directionball.y = 0;
  while(game.directionball.x==0 || game.directionball.y==0){
    game.directionball.x=Math.floor((Math.random()*3)-1);
    game.directionball.y=Math.floor((Math.random()*3)-1);
  }
}


 function updateGame (socket) {
   console.log('update game')
   let game = games[socket.room]
   timegame[socket.room]+=1;
   if(timegame[socket.room]<100){
     var getready = (0 + timegame[socket.room])+"%";
     socket.emit('getReady', {
       getready
     });
     socket.broadcast.emit('getReady', {
       getready
     });
     return;
   } else
   if (game.scores.p2<10 && game.scores.p1<10) {
    if(ballReseted[socket.room]==false){
      resetBall(socket);
      ballReseted[socket.room] = true;
    }
    game.ball.x+=game.directionball.x*game.speed;
    game.ball.y+=game.directionball.y*game.speed;
    if(game.ball.x<=0 || game.ball.x>=1 ){
      if(game.ball.x<=0){
        game.scores.p2+=1;
      }else{
        game.scores.p1+=1;
      }
      resetBall(socket);
    }
    if(game.ball.y<=0 || game.ball.y>=1 ){
      game.directionball.y=-game.directionball.y
    }
    if(
      intersects(
      {
        x:0.1,
        y:game.player1,
        width:0.025,
        height:0.2
      },
      {
        x:game.ball.x,
        y:game.ball.y,
        width:0.01,
        height:0.01
      })
     ||
     intersects(
     {
      x:0.87,
      y:game.player2,
      width:0.025,
      height:0.2
    },
    {
      x:game.ball.x,
      y:game.ball.y,
      width:0.01,
      height:0.01
    }
    )
    ){
      game.speed+=0.001
      game.directionball.x=-game.directionball.x
    }

    io.sockets.in(socket.room).emit('updateGame', {
      game
    });
    console.log(game)
   }
   else {
     if(game.scores.p1 == 10)
      game.winner = playerNames[socket.room][0]
      else if (game.scores.p2 == 10)
      game.winner = playerNames[socket.room][0]
    io.sockets.in(socket.room).emit('updateGame', {
        game
    });
    ballReseted[socket.room] = false;
    emitingGame[socket.room] = false;
    timegame[socket.room] =0 
    games[socket.room] = {
      directionball:{x:1,y:1},
      ball:{x:0.5,y:0.5},
      player1:0.5,
      player2:0.5,
      scores:{p1:0,p2:0},
      speed: 0.01
    }
    clearInterval(gameInterval[socket.room]);
   }
   console.log(game)
  }

io.on('connection', function(socket){
  number = number+1
  console.log('a user connected / Num :' + number );

  
  socket.on ('verify', (username,fn)=> {
    let exist = false
    let rooms = io.sockets.adapter.rooms;
    for (let room in rooms) {
      for (let client in playerNames[room]) {
        if(playerNames[room][client] == username)
          exist = true 
      }
    }
    fn(exist) 
  })

  socket.on('addUser',function(username){
    socket.username = username;
    console.log('NAAAAAAAAAAAAAAAAAAAAAAME '+socket.username)
});

socket.on('playerUp', (username) => {
  let game = games[socket.room]
  if(username==playerNames[socket.room][0]){
    if(game.player1<=0)return;
    game.player1-=0.02
  }else if (username==playerNames[socket.room][1]) {
    if(game.player2<=0)return;
    game.player2-=0.02
  }
});

socket.on('playerDown', (username) => {
  let game = games[socket.room]
  if(username==playerNames[socket.room][0]){
    if(game.player1>=0.8)return;
    game.player1+=0.02
  }else if (username==playerNames[socket.room][1]) {
    if(game.player2>=0.8)return;
    game.player2+=0.02
  }

});

socket.on('leave', ()=> {
  playerNames[socket.room] =  playerNames[socket.room].filter(player => player!==socket.username)
  socket.leave(socket.room)
})

socket.on('replay', (username,fn)=> {
  console.log(username)
  if (replayPlayers[socket.room])
  replayPlayers[socket.room].push(username)
  else replayPlayers[socket.room] = [username]
  console.log(replayPlayers[socket.room])
  if (replayPlayers[socket.room].length == 2){
    gameInterval[socket.room] = setInterval(() => {
      updateGame(socket);
    }, 50);
    emitingGame[socket.room] = true;
    fn(2)
  }
  if (playerNames[socket.room].length == 1)
  fn(1)
})

socket.on('join', () => {
  let rooms = io.sockets.adapter.rooms;
  let available = false
  console.log(rooms)
  console.log('///////')
  socket.room = socket.id

  // if any of the current rooms have only one
  // player, join that room.
  for (let room in rooms) {
      if ( io.of('/').adapter.rooms[room].length == 1 && !available) {
        available = true
        socket.join(room);
        socket.room = room

        if (playerNames[room]) {
          playerNames[room].push(socket.username)
          emitingGame[room] = false
          ballReseted[room] = false
          timegame[room] = 0
          console.log(playerNames[room])
        }

        else playerNames[room] = [socket.username]

        if (io.of('/').adapter.rooms[room].length == 2) {
          games[room] = { 
              directionball:{x:1,y:1},
              ball:{x:0.5,y:0.5},
              player1:0.5,
              player2:0.5,
              scores:{p1:0,p2:0},
              speed: 0.01
            }
          io.sockets.in(room).emit('connectToRoom', {players:2, names:playerNames[room]})
          if(emitingGame[room]==false){
            gameInterval[room] = setInterval(() => {
              updateGame(socket);
            }, 50);
            emitingGame[room] = true;
          }
        }

        if (room!==socket.id) {
          socket.leave(socket.id);
        }

  }
} 

  console.log(rooms)
})

socket.on('joinOther', () => {
  playerNames[socket.room] =  playerNames[socket.room].filter(player => player!==socket.username)
  socket.leave(socket.room)
  let Oldromm = socket.room
  let rooms = io.sockets.adapter.rooms;
  let available = false
  console.log(rooms)
  console.log('///////')
  socket.room = socket.id

  // if any of the current rooms have only one
  // player, join that room.
  for (let room in rooms) {
      if ( io.of('/').adapter.rooms[room].length == 1 && !available && room!==Oldromm) {
        available = true
        socket.join(room);
        socket.room = room

        if (playerNames[room]) {
          playerNames[room].push(socket.username)
          emitingGame[room] = false
          ballReseted[room] = false
          timegame[room] = 0
          console.log(playerNames[room])
        }

        else playerNames[room] = [socket.username]

        if (io.of('/').adapter.rooms[room].length == 2) {
          games[room] = { 
              directionball:{x:1,y:1},
              ball:{x:0.5,y:0.5},
              player1:0.5,
              player2:0.5,
              scores:{p1:0,p2:0},
              speed: 0.01
            }
          io.sockets.in(room).emit('connectToRoom', {players:2, names:playerNames[room]})
          if(emitingGame[room]==false){
            gameInterval[room] = setInterval(() => {
              updateGame(socket);
            }, 50);
            emitingGame[room] = true;
          }
        }

        if (room!==socket.id) {
          socket.leave(socket.id);
        }

  }
} 

  console.log(rooms)
})

    socket.on('disconnect', function(){
    console.log('user disconnected');

    playerNames[socket.room] =  playerNames[socket.room].filter(player => player!==socket.username)
    io.sockets.in(socket.room).emit('playerLeft', {players:playerNames,Left:socket.username})
    ballReseted[socket.room] = false;
    emitingGame[socket.room] = false;
    timegame[socket.room] = 0
    games[socket.room] = {
      directionball:{x:1,y:1},
      ball:{x:0.5,y:0.5},
      player1:0.5,
      player2:0.5,
      scores:{p1:0,p2:0},
      speed: 0.01
    }
    clearInterval(gameInterval[socket.room]);
  });
});
