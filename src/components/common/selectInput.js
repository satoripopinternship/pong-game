import React from 'react';
import PropTypes from 'prop-types';

const SelectInput = ({name, label , onChange , value , defaultOption, options, errors}) => {

return (
    <div className="form-group">
        <label htmlFor={name}>{label}</label>
        <div className='field'>
        <select name={name} onChange={onChange} value={value}>
            <option>{defaultOption}</option>
             {options.map(option => {
                return (<option key={option.key}>{option}</option>)
            })}
        </select>
        {errors && <div className="alert alert-danger">{errors}</div>}
        </div>
    </div>
)
}

SelectInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    defaultOption : PropTypes.string.isRequired,
    options: PropTypes.arrayOf(PropTypes.object).isRequired,
    value: PropTypes.string,
    errors: PropTypes.string
}

export default SelectInput;