import React from 'react';
import Game from '../game/gamePage'
import { PropTypes } from 'prop-types';
import image from '../../img/pong.png'


class HomePage extends React.Component {
    
    constructor(props,context) {
        super(props,context);
        this.state = {
            username:'',
            connected: false
        }
        this.changeName = this.changeName.bind(this)
        this.playGame = this.playGame.bind(this)
    }

    playGame () {
        const { socket } = this.props;
        socket.emit("verify",this.state.username, (exist)=>{
            console.log("Client emit verify")
            if (!exist) {
                socket.username = this.state.username
                this.setState( {connected:true})
                socket.emit('addUser',this.state.username);
                socket.emit('join');
            }
            else {
                window.alert("This pseudo exists")
            }
        })

    }
 
    changeName (e) {
        this.setState({username:e.target.value})
    }


    render (){
        return (
            <div>
            {!this.state.connected ? 
            <section className="principal" id="principal">
            <img src={image} className="image" />
              <h1>PING PONG</h1>
              <input type="text" name="" id="username" required value={this.state.username} onChange={this.changeName} placeholder="YOUR NAME"/>
              <button type="button" id='butt' disabled={this.state.username.length==0} name="button" onClick={this.playGame}>PLAY !</button>
            </section>:
            <Game socket={this.props.socket}/>
            }
            </div>
           
        );
    }
}

HomePage.propTypes = {
    socket: PropTypes.object.isRequired
}


export default HomePage;
