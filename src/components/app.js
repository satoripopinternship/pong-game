import React from 'react';
import HomePage from './home/homePage';
import { PropTypes } from 'prop-types';
import io from 'socket.io-client'
var socket = io();
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons'


library.add(faStroopwafel)
    
class App extends React.Component {
    constructor (props) {
        super (props)
    }
    render (){
        return (
            <div className="container-fluid">
            <header>
            </header>
            <HomePage socket={socket}/>
            </div>
        );
    }
}


export default App;
