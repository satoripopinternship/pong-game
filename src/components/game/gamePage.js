import React from 'react';
import { PropTypes } from 'prop-types';
import { ReactDOM } from 'react-dom';
import SelectInput from '../common/selectInput';


class GamePage extends React.Component {

    constructor(props,context) {
 
        super(props,context)
        // this.canvas = document.getElementById("canvas");

        this.state = {
            waiting : true,
            left:null,
            playersNames : [],
            winner:null,
            end:false,
            username : this.props.socket.username,
            game : {
              player1 : 0.5,
              player2 : 0.5,
              ball : {x:0.5,y:0.5},
              scores : {p1:0,p2:0}
            },
            room : null
        }
        this.updateGameSocket = this.updateGameSocket.bind(this)
        this.drawGame = this.drawGame.bind(this)
        this.replay = this.replay.bind(this)
        this.playOther = this.playOther.bind(this)
        // this.choose = this.choose.bind(this)

        const { socket } = this.props;
        const {game} = this.state

      //   socket.on('receivePlayers',(data) => {
      //     console.log(data.rooms)
      //     let rooms = data.rooms.filter(function(item) {
      //       return item !== socket.id
      //     })
          
      //     this.setState({waiting:true,rooms})
      //  });

        socket.on('connectToRoom',(data) => {
          console.log(data)
            if (data.players ==1 ) 
            this.setState({waiting:true})
            if (data.players ==2 ) 
            {
            this.setState({waiting:false,playersNames:data.names,room:data.room})
            socket.emit('sendGame',{game:data.game,room:data.room,playersNames:data.names});
            console.log(this.state.playersNames)
            // this.drawGame()
            // socket.on('getReady', (data) =>{
                // this.drawGetReady(data.getready);
            // });
              if (game.scores.p1<10 && game.scores.p2<10) {
                document.addEventListener("keydown", (e) => {
                  // console.log(e.key);
                    if(e.key=='ArrowUp' || e.key=='ArrowDown'){
                      this.move(e.key);
                    }                
                })    
          
                  socket.on('updateGame',(res) => {
                    socket.emit('update',{game:res.game,room:res.room});
                    this.updateGameSocket(res.game)
                      if(res.game.winner && (res.game.scores.p1==10 || res.game.scores.p2==10)) {
                        this.setState({end:true,winner:res.game.winner})
                      }                     
                  });   
              } 
            }
         }); 

     
         socket.on('playerLeft',(data) => {
            this.setState({waiting:true,left:data.Left})
         });
    }

    drawGetReady(time){
      this.canvas = document.querySelector("canvas");
      this.ctx = this.canvas.getContext('2d')
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.ctx.save();
        this.ctx.fillStyle="black";
        this.ctx.fillRect(0,0,this.canvas.width,this.canvas.height);
        this.ctx.fillStyle="white";
        this.ctx.font = "50px Arial bold";
        this.ctx.fillText(""+time,this.canvas.width/2,this.canvas.height/2);
      this.ctx.restore();
    }

  //   choose (ev){
  //     const { socket } = this.props;
  //     socket.emit('joinRoom', ev.target.value)
  // }


    drawGame(){
      const {game} = this.state

      this.canvas = document.querySelector("canvas");
      this.ctx = this.canvas.getContext('2d')
  
  
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.ctx.save();
        this.ctx.fillStyle="black";
        this.ctx.fillRect(0,0,this.canvas.width,this.canvas.height);
        this.ctx.fillStyle="white";
        this.ctx.fillRect(this.canvas.width*0.1,this.canvas.height*game.player1,this.canvas.width*0.025,this.canvas.height*0.2);
        this.ctx.fillRect(this.canvas.width*0.9,this.canvas.height*game.player2,-this.canvas.width*0.025,this.canvas.height*0.2);
        this.ctx.fillRect(this.canvas.width*game.ball.x,this.canvas.height*game.ball.y,this.canvas.width*0.01,this.canvas.height*0.01);
        this.ctx.font = "30px Arial";
        this.ctx.fillText(""+game.scores.p1,this.canvas.width*0.1,this.canvas.height*0.1);
        this.ctx.fillText(""+game.scores.p2,this.canvas.width*0.9,this.canvas.height*0.1);
        this.ctx.fillText(""+this.state.playersNames[0],this.canvas.width*0.1,this.canvas.height*0.95);
        this.ctx.fillText(""+this.state.playersNames[1],this.canvas.width*0.8,this.canvas.height*0.95);          // this.ctx.arc(this.canvas.width*this.ball.x, this.canvas.height*this.ball.y, this.canvas.height*0.01, 0, 2 * Math.PI);
        // this.ctx.fill();
      this.ctx.restore();
    }

    updateGameSocket(game){
      this.setState({
        game
    })
        this.drawGame();
      }



    // componentWillUnmount () {
    //   document.removeEventListener("keydown",false)
    // }

    
      move(key){
        const {username} = this.state
        const { socket } = this.props;
        if(username == this.state.playersNames[1]){
          if(key=='ArrowUp' ){
            this.setState((prevState) => {
              return {game:{player1: prevState.player1 - 0.02}};
            });
            socket.emit('playerUp',{username,playersNames:this.state.playersNames,room:this.state.room});
          }else if (key=='ArrowDown') {
            this.setState((prevState) => {
              return {game:{player1: prevState.player1 + 0.02}};
            });
            socket.emit('playerDown',{username,playersNames: this.state.playersNames,room:this.state.room});
          }
    
        }else if (username == this.state.playersNames[0] ) {
          if(key=='ArrowUp'){
            this.setState((prevState) => {
              return {game:{player2: prevState.player2 - 0.02}};
            });
            socket.emit('playerUp',{username,playersNames: this.state.playersNames,room:this.state.room});
          }else if (key=='ArrowDown') {
            this.setState((prevState) => {
              return {game:{player2: prevState.player2 + 0.02}};
            });
            socket.emit('playerDown',{username,playersNames: this.state.playersNames,room:this.state.room});
          }
        }
      }

      replay () {
        this.setState({end:false,winner:null})
        this.props.socket.emit('replay',this.state.username, (number) => {
          if (number==1) {
            this.setState({waiting:true})
            this.props.socket.emit('join')
          }
        })
        console.log(this.state.username)
      }

      playOther () {
        this.setState({end:false,winner:null,waiting:true})
        this.state.playersNames = []
        this.props.socket.emit('joinOther')
      }


    render (){
        return (
            <section className="game" id="game">
              {this.state.waiting?
              <div className='' >
                  <div className="centered loader"></div>
                  {this.state.left?<h3> <strong>{this.state.left}</strong> Has left the game</h3>: null}
                  <h3>Waiting for a player to join</h3> 
                  {/* <SelectInput name="Rooms" label="Rooms" onChange={this.choose} defaultOption="Choose" options={this.state.rooms}/>    */}
              </div>:
              <div>
              <h1 className='fontB'>PING PONG</h1>
              <h4>Use arrows keys : <i className="far fa-arrow-alt-circle-up fa-lg"></i> <i className="far fa-arrow-alt-circle-down fa-lg"></i></h4>
              <h4>10 points to win!</h4>

              <div>
                {!this.state.end?
                  <canvas id="#canvas" width='1024px' height='720px'></canvas>:
                  <div>
                  <h2>{this.state.winner} is the winner !</h2>
                  <button type="button" className="btn btn-danger btn-lg" style={{marginRight:10}} onClick={this.replay}>Replay</button>
                  <button type='button' className='btn btn-primary btn-lg' onClick={this.playOther}>Play with an other</button>
                  </div>
                }
              </div>
              </div>
              }
            </section>
        );
    }
}





GamePage.propTypes = {
    socket: PropTypes.object.isRequired
}
export default GamePage;
